// Function parameters are variables that wait for a value from an argument in the function invocation
function sayMyName(name) {
	console.log('My name is ' + name);
};

// Function arguments are values like strings, numbers, etc. that you can pass onto the function you are invoking
sayMyName('RuRu Sensei');

// You can reuse functions by invoking them at any time from anywhere in your code. Make sure you have declared them first
sayMyName('Darien');
sayMyName('Kevin');


// You can also pass variables as arguments to a function
let myName = 'Randy';
sayMyName(myName);


// You can use arguments and parameters to make your data inside your function dynamic (changing)
function sumOfTwoNumbers(firstNumber, secondNumber) {
	let sum = 0;
	sum = firstNumber + secondNumber;
	console.log('The sum of the first number and second number is ' + sum);
};

sumOfTwoNumbers(9, 16);
sumOfTwoNumbers(12, 10)
sumOfTwoNumbers(1991, 1992)


// You can pass a fuction as an argument for another function.
function argumentFunction() {
	console.log('This is a function that was passed as an argument')
}

function parameterFunction(argumentFunction) {
	argumentFunction()
}

parameterFunction(argumentFunction)


// If you add an extra or you lack an argument javascript won't show an error message, it will simply be simply undefined
function displayFullName(firstName, middleName, lastName) {
	console.log('Your full name is: ' + firstName + ' ' + middleName + ' ' + lastName)
}

displayFullName('Darien', 'Dimatulac', 'Piedraverde')
displayFullName('Johnny', 'B.', 'Goode', 'yeah')

// Example of string interpolation (pag-inject)
function displayMovieDetails(title, synopsis, director){
	console.log(`The movie is entitled ${title}`)
	console.log(`The synopsis is ${synopsis}`)
	console.log(`The director is ${director}`)
}

displayMovieDetails('Weathering with you', 'About love and weather', 'Makoto Shinkai')

// single quotations vs double quotations - it's best to use double quotation when your string uses an aposthrope
// console.log('Howl's moving castle)
// console.log("Howl's moving castle")



function displayPlayerDetails(name, age, playerClass) {
	/*console.log(`Palyer name: ${name}`)
	console.log(`Palyer age: ${age}`)
	console.log(`Palyer playerClass: ${playerClass}`)*/
	let playerDetails = `Name: ${name}, Age: ${age}, Class: ${playerClass}`
	return playerDetails
	console.log('Hello!')
}

let variable = 'a variable'	

console.log(displayPlayerDetails('Elsworth', '120', 'Paladin')) 





